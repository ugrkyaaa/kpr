# Before running:

Run this command to install required node modules:
```
npm install
```

# Running: 
```
npm run start
```

The SCSS files are automatically compiled into CSS. If the styles are not loading, please run the command to compile SCSS into CSS:
```
npm run sass-preprocess
```