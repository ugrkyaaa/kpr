const express = require('express');
const app = express();
const path = require('path');

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.use(express.static('public'))

const port = process.env.PORT || 8000;

app.listen(port, function() {
  console.log('Server is on ' + port)
})